<?php
defined('_JEXEC') or die;
JHTML::_('behavior.mootools');
$templateUrl = $this->baseurl."/templates/".$this->template."/";
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl-pl" lang="pl-pl" dir="ltr">
<head>
<jdoc:include type="head" />
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="description" content="">
  <!-- Mobile viewport optimized: h5bp.com/viewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
  <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
  <link rel="stylesheet" href="<?php echo $templateUrl;?>css/bootstrap.css">
  <!-- More ideas for your head here: h5bp.com/d/head-Tips -->
  
  <!-- Want to try a different theme?
  	   If not using LESS compiler: Uncomment below
       and change to name of desired theme. (See docs) 	-->
  <!--link type="text/css" rel="stylesheet" href="<?php echo $templateUrl;?>extras/css/amelia.css" /-->

  <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
  <script src="<?php echo $templateUrl;?>extras/h5bp/js/libs/modernizr-2.5.3.min.js"></script>
  <style>
  body {
    padding-top: 60px;
  }
</style>
</head>
<body>
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]>
    <link href="<?php echo $templateUrl;?>css/bootstrap.ie6.css" rel="stylesheet">
  	<p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p>
  <![endif]-->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">Kickstrap joomla template</a>
          <div class="nav-collapse">
          <jdoc:include type="modules" name="top-menu" style="none" />
          <!-- 
            <ul class="nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
            </ul>
             -->
            <p class="navbar-text pull-right">
            <jdoc:include type="modules" name="top-login" style="none" />
            Logged in as <a href="#">username</a></p>
          </div><!-- !.nav-collapse -->
        </div>
      </div>
    </div>

<div class="container">  
  <header>
  
  </header>
  
  <div role="main">
    <jdoc:include type="component" />
  </div>
  
  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php echo $templateUrl;?>extras/h5bp/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
  <!-- scripts concatenated and minified via ant build script-->
  <script src="<?php echo $templateUrl;?>extras/prettify/prettify.js"></script>
  <script src="<?php echo $templateUrl;?>js/bootstrap.min.js"></script>
  
  <!-- end scripts-->
  
  <!-- If you'd rather pick and choose, comment the above and uncomment what you need below -->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-alert.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-button.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-carousel.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-collapse.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-dropdown.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-modal.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-modal.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-modal.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-popover.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-scrollspy.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-tab.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-tooltip.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-transition.min.js"></script-->
  <!--script type="text/javascript" src="<?php echo $templateUrl;?>js/bootstrap-typeahead.min.js"></script-->
  <!--script type="text/javascript" src=""></script-->
  
  <!-- end scripts -->

  <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       mathiasbynens.be/notes/async-analytics-snippet -->

  <script type="text/javascript">
  // Use the modernizr.load() function to run polyfills for older browsers.
    Modernizr.load({

    });
  </script>
    <footer>
    <p><i class="icon-github-sign"></i> Built with <a href="http://ajkochanowicz.github.com/Kickstrap/">Kickstrap</a></p>
  </footer>
</div>
  <!-- A little ie7- magic -->
  <script type="text/javascript"> $(function(){if($.browser.msie&&parseInt($.browser.version,10)===6){$('.row div[class^="span"]:last-child').addClass("last-child");$('[class="span"]').addClass("margin-left-20");$(':button[class="btn"], :reset[class="btn"], :submit[class="btn"], input[type="button"]').addClass("button-reset");$(":checkbox").addClass("input-checkbox");$('[class^="icon-"], [class=" icon-"]').addClass("icon-sprite");$(".pagination li:first-child a").addClass("pagination-first-child")}}) </script>
  <jdoc:include type="modules" name="debug" style="none" />
</body>
</html>