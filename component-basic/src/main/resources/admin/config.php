<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die;
class SkelConfig {
	public static $version = '${project.extension.version}';
	public static $buildNumber = '${ci.buildnumber}';
	public static $buildName = '${ci.buildname}';
}
?>
