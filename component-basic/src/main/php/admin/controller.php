<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

/**
 * General Controller
 */
class SkelController extends JControllerLegacy {
	function display($cachable = false, $urlparams = false) {
		// set default view if not set
		JRequest::setVar('view', JRequest::getCmd('view', 'main'));
		$view = JRequest::getCmd('view', 'main');
		parent::display($cachable, $urlparams);
		// add Sub menu
		SkelHelper::addSubmenu($view);
	}
}