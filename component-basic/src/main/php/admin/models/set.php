<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modeladmin');

class SkelModelSet extends JModelAdmin {
	protected function allowEdit($data = array(), $key = 'id') {
		return JFactory::getUser()->authorise('core.edit', 'com_skel.set.'.((int) isset($data[$key]) ? $data[$key] : 0)) or parent::allowEdit($data, $key);
	}
	public function getTable($type = 'Set', $prefix = 'SkelTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = true) {
		$form = $this->loadForm('com_skel.item', 'set', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}
		return $form;
	}

	protected function loadFormData() {
		$data = JFactory::getApplication()->getUserState('com_skel.edit.set.data', array());
		if (empty($data))
		{
			$data = $this->getItem();
		}
		return $data;
	}

	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {
			// Convert the params field to an array.
			$registry = new JRegistry;
			$registry->loadString($item->navi_dots_conf);
			$item->navi_dots_conf = $registry->toArray();
			$registry = new JRegistry;
			$registry->loadString($item->preloader_conf);
			$item->preloader_conf = $registry->toArray();
		}

		return $item;
	}

}
