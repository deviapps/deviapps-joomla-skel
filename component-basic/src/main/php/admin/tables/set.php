<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.database.table');

class SkelTableSet extends JTable {
	function __construct(&$db) {
		if ($db == NULL) {
			$db = JFactory::getDbo();
		}
		parent::__construct('#__da_sliderset', 'id', $db);
	}
	
	public function bind($array, $ignore = '') {
		if (isset($array['navi_dots_conf']) && is_array($array['navi_dots_conf'])) {
			$registry = new JRegistry();
			$registry->loadArray($array['navi_dots_conf']);
			$array['navi_dots_conf'] = (string) $registry;
		}

		if (isset($array['preloader_conf']) && is_array($array['preloader_conf'])) {
			$registry = new JRegistry();
			$registry->loadArray($array['preloader_conf']);
			$array['preloader_conf'] = (string) $registry;
		}

		return parent::bind($array, $ignore);
	}
}