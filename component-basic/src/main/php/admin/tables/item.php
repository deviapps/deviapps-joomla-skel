<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.database.table');

class SkelTableItem extends JTable {
	function __construct(&$db) {
		if ($db == NULL) {
			$db = JFactory::getDbo();
		}
		parent::__construct('#__da_slideritem', 'id', $db);
	}
	public function bind($array, $ignore = '') {
		if (isset($array['attributes']) && is_array($array['attributes'])) {
			$registry = new JRegistry();
			$registry->loadArray($array['attributes']);
			$array['attributes'] = (string) $registry;
		}
		return parent::bind($array, $ignore);
	}
	
}