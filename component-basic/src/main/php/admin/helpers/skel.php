<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die;
class SkelHelper {
	/**
	 * Configure the submenu linkbar.
	 */
	public static function addSubmenu($submenu) {
		JSubMenuHelper::addEntry(JText::_('COM_SKEL_SUBMENU_MAIN'), 'index.php?option=com_skel', $submenu == 'main');
		JSubMenuHelper::addEntry(JText::_('COM_SKEL_SUBMENU_SETS'), 'index.php?option=com_skel&view=sets', $submenu == 'sets');
		JSubMenuHelper::addEntry(JText::_('COM_SKEL_SUBMENU_ITEMS'), 'index.php?option=com_skel&view=items', $submenu == 'items');
	}
}
