<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * Main view
 */
class SkelViewMain extends JViewLegacy {
	function display($tpl = null) {
		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	protected function addToolBar()  {
		JToolBarHelper::title(JText::_('COM_SKEL_MAIN'), 'generic.png');
		JToolBarHelper::preferences('com_skel');
	}
	/**
	 * Method to set up the document properties
	 * @return void
	 */
	protected function setDocument() {
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_SKEL_ADMINISTRATION'));
	}
}
