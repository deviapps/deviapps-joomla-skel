<?php
/**
 * @version		: edit.php 2011-09-20 06:09:08$
 * @author		Cyprian Sniegota 
 * @package		Bzstudy
 * @copyright	Copyright (C) 2011- Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
$params = $this->form->getFieldsets('params');
?>
<form action="<?php echo JRoute::_('index.php?option=com_skel&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
        <fieldset class="adminform">
                <legend><?php echo JText::_( 'COM_SKEL_ITEM_EDIT' ); ?></legend>
                <ul class="adminformlist">
<?php foreach($this->form->getFieldset() as $field): ?>
                        <li><?php echo $field->label;echo $field->input;?></li>
<?php endforeach; ?>
                </ul>
        </fieldset>
        <div>
                <input type="hidden" name="task" value="item.edit" />
                <?php echo JHtml::_('form.token'); ?>
        </div>
</form>
