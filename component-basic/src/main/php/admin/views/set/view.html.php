<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class SkelViewSet extends JViewLegacy {
	public function display($tpl = null) {
		// get the Data
		$form = $this->get('Form');
		$item = $this->get('Item');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign the Data
		$this->form = $form;
		$this->item = $item;

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	protected function addToolBar() {
		JRequest::setVar('hidemainmenu', true);
		$user = JFactory::getUser();
		$userId = $user->id;
		$isNew = $this->item->id == 0;
		JToolBarHelper::title(JText::_('COM_SKEL_MANAGER_SET'), 'generic.png');

    // Built the actions for new and existing records.
		if ($isNew) {
			JToolBarHelper::apply('set.apply', 'JTOOLBAR_APPLY');
			JToolBarHelper::save('set.save', 'JTOOLBAR_SAVE');
			JToolBarHelper::custom('set.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
			JToolBarHelper::cancel('set.cancel', 'JTOOLBAR_CANCEL');
		} else {
			// We can save the new record
			JToolBarHelper::apply('set.apply', 'JTOOLBAR_APPLY');
			JToolBarHelper::save('set.save', 'JTOOLBAR_SAVE');
			JToolBarHelper::custom('set.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
			JToolBarHelper::custom('set.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
			JToolBarHelper::cancel('set.cancel', 'JTOOLBAR_CLOSE');
		}
	}

}
