<?php
/**
 * @version		: default_foot.php 2011-12-03 15:12:03$
 * @author		Cyprian Sniegota 
 * @package		Questionnaire
 * @copyright	Copyright (C) 2011- Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<tr>
	<td colspan="4"><?php echo $this->pagination->getListFooter(); ?></td>
</tr>