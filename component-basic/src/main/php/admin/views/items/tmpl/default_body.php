<?php
/**
 * @version		: default_body.php 2011-12-03 15:12:03$
 * @author		Cyprian Sniegota
 * @package		Questionnaire
 * @copyright	Copyright (C) 2011- Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

$user		= JFactory::getUser();
$userId		= $user->get('id');
?>
<?php foreach($this->items as $i => $item):
$canEdit	= $user->authorise('core.edit',			'com_skel.set.'.$item->id);
$canEditOwn	= $user->authorise('core.edit',			'com_skel.set.'.$item->id); //TODO
$canCheckin	= $user->authorise('core.manage',		'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
$canChange	= $user->authorise('core.edit.state',	'com_skel.set.'.$item->id) && $canCheckin;
$editLink	= JRoute::_("index.php?option=com_skel&task=item.edit&id=".$item->id);
$attributes = new JRegistry($item->attributes);
?>
<tr class="row<?php echo $i % 2; ?>">
	<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?>
	</td>
	<td><?php if ($item->checked_out) : ?> <?php
	$editor = JFactory::getUser($item->checked_out); 
	$item->editor = $editor->name;
	echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'items.', $canCheckin); ?>
	<?php endif; ?> <?php if ($canEdit || $canEditOwn) : ?> <a href="<?php echo $editLink;?>"> <?php echo $this->escape($item->name); ?>
	</a> <?php else : ?> <?php echo $this->escape($item->name); ?> <?php endif; ?>
		<p class="smallsub">
		<?php echo "(".$this->escape($attributes->get('url')).")";?>
		</p>
	</td>
	<td class="center"><?php echo JHtml::_('jgrid.published', $item->published, $i, 'items.', $canChange, 'cb'); ?>
	</td>
	<td><?php echo $item->id; ?>
	</td>
</tr>
		<?php endforeach; ?>