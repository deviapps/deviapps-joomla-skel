<?php
/**
 * @version		: default_body.php 2011-12-03 15:12:03$
 * @author		Cyprian Sniegota 
 * @package		Questionnaire
 * @copyright	Copyright (C) 2011- Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

$user		= JFactory::getUser();
$userId		= $user->get('id');
?>
<?php foreach($this->items as $i => $item):
		$canEdit	= $user->authorise('core.edit',			'com_skel.set.'.$item->id); 
		$canCheckin	= $user->authorise('core.manage',		'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
		$canChange	= $user->authorise('core.edit.state',	'com_skel.set.'.$item->id) && $canCheckin;
		$editLink	= "index.php?option=com_skel&task=set.edit&id=".$item->id;
?>
	<tr class="row<?php echo $i % 2; ?>">
		<td class="center">
			<?php echo JHtml::_('grid.id', $i, $item->id); ?>
		</td>
		<td>
			<?php if ($canEdit):?>
				<a href="<?php echo $editLink;?>">
					<?php echo $item->name; ?>
				</a>
			<?php else:?>
				<?php echo $item->name; ?>
			<?php endif;?>
		</td>
		<td class="center">
			<?php echo JHtml::_('jgrid.published', $item->published, $i, 'articles.', $canChange, 'cb'); ?>
		</td>
		<td>
			<?php echo $item->id; ?>
		</td>
	</tr>
<?php endforeach; ?>