<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted Access');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
?>
<tr>
	<th width="1%"><input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>"
		onclick="Joomla.checkAll(this)" />
	</th>
	<th><?php echo JHtml::_('grid.sort', 'JGLOBAL_TITLE', 's.name', $listDirn, $listOrder); ?>
	</th>
	<th width="5%"><?php echo JHtml::_('grid.sort', 'JSTATUS', 's.published', $listDirn, $listOrder); ?>
	</th>
	<th width="5"><?php echo JText::_('JGRID_HEADING_ID'); ?>
	</th>
</tr>

