<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * Sets view
 */
class SkelViewSets extends JViewLegacy {
	function display($tpl = null) {
		$this->addToolBar();
		$model	= $this->getModel();
		/* load model data */
		$this->state		= $this->get('State');
		$this->items 		= $this->get('Items');
		$this->pagination 	= $this->get('Pagination');
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	protected function addToolBar()  {
		JToolBarHelper::title(JText::_('COM_SKEL_SETS'), 'generic.png');
		JToolBarHelper::addNew('set.add', 'JTOOLBAR_NEW');
		JToolBarHelper::editList('set.edit', 'JTOOLBAR_EDIT');
// 		JToolBarHelper::deleteListX('', 'sets.delete', 'JTOOLBAR_DELETE');
	}
	/**
	 * Method to set up the document properties
	 * @return void
	 */
	protected function setDocument() {
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_SKEL_ADMINISTRATION'));
	}
}
