<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * @author cyprian
 *
 */
class SkelViewItems extends JViewLegacy {
	protected $state;
	protected $user;
	protected $items;
	protected $params;

	function display($tpl = null) {
		$app		= JFactory::getApplication();
		$this->user = JFactory::getUser();
		$model 		= $this->getModel();
		$this->params = $app->getParams();
		$this->items = $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state = $this->get('State');
		return parent::display($tpl);
	}

}