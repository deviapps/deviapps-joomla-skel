<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');
class SkelModelData extends JModelLegacy {
	private $_cache = array();
	public function getItems($setId) {
		$cacheId = 'items';
		if (! isset($this->_cache[$cacheId])) {
			JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_skel'.DS.'tables');
			$set = $this->getSet($setId);
			if ($set == null) {
				$this->setError("Error loading set");
				return null;
			}
			$itemids = explode(',', $set->items);
			$items = array();
			foreach ($itemids as $itemid) {
				$item = JTable::getInstance('Item','SkelTable');
				$item->load($itemid);
				$registry = new JRegistry;
				$registry->loadString($item->attributes);
				$item->attributes = $registry;
				if ($item->published == 1) {
					$items[] = $item;
				}
			}
			$this->_cache[$cacheId] = $items;
		}
		return $this->_cache[$cacheId];
	}

	public function getSet($setId) {
		$cacheId = 'set';
		if (! isset($this->_cache[$cacheId])) {
			$db = JFactory::getDbo();
			JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_skel'.DS.'tables');
			$set = JTable::getInstance('Set','SkelTable');
			$set->load($setId);
			$registry = new JRegistry;
			$registry->loadString($set->navi_dots_conf);
			$set->navi_dots_conf = $registry;
			$registry = new JRegistry;
			$registry->loadString($set->preloader_conf);
			$set->preloader_conf = $registry;
			if ($set->published == 1) {
				$this->_cache[$cacheId] = $set;
			} else {
				$this->_cache[$cacheId] = null;
			}
		}
		return $this->_cache[$cacheId];
	}

}