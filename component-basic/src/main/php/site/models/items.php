<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');
class SkelModelItems extends JModelList {
	
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'name'
			);
		}
		parent::__construct($config);
	}
	
	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication('administrator');
		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		$params = JComponentHelper::getParams('com_skel');
		$this->setState('params', $params);
		$filterState = JRequest::getVar('filter_state');
		$this->setState('filter.state',$filterState);
		
		parent::populateState('name', 'asc');
	}
	
	protected function getListQuery() {
		$user	= JFactory::getUser();
	
		// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
	
		// Select required fields from the categories.
		$query->select('s.*');
		$query->from('`#__da_slideritem` AS s');
		
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$query->where('s.name = '.$db-quote($search));
		}
		
		$filterState = $this->getState('filter.state');
		if ($filterState != '') {
			$query->where('s.state='.$db->quote($filterState));
		}
		
		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering');
		$orderDirn	= $this->state->get('list.direction');
		$query->order($db->getEscaped($orderCol.' '.$orderDirn));
		/* echo (string) $query;
		$db->setQuery($query);
		$db->query();
		print_r($db); */
		return $query;
	}
	
}