<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class SkelController extends JController {
	public function display($cachable = false, $urlparams = false) {
		return parent::display($cachable,$urlparams);
	}
}
