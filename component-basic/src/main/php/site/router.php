<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');

function SkelBuildRoute( &$query ) {
	$segments = array();
/*	if(isset($query['setid'])) {
		$segments[] = $query['setid'];
		unset( $query['setid'] );
	}; */
	return $segments;
}

function SkelParseRoute( $segments ) {
	$vars = array();
/*	if (isset($segments[0])) {
		$vars['setid'] = $segments[0]; 
		$vars['task'] = 'data.xml';
	}
	*/
	return $vars;
}
