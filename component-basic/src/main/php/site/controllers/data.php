<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');
class SkelControllerData extends JControllerLegacy {
	public function store() {
		$session = JFactory::getSession();
		$sliderId= JRequest::getVar('sliderId');
		$elementId = JRequest::getVar('elementId');
		$time = JRequest::getVar('time');
		$session->set('skel.stamp.id',$sliderId);
		$session->set('skel.stamp.elid',$elementId);
		$session->set('skel.stamp.time',$time);
		echo "ok\n$sliderId, $elementId, $time";
		exit;
	}

	public function xml() {
		$session = JFactory::getSession();
		header('Content-Type'. ': ' . 'application/xml' .  '; charset=' . 'utf-8');
		echo '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";
		$setId 	= JRequest::getInt('setid',0);
		if ($setId == 0) {
			echo "<error>Bad setid parameter</error>";
			exit;
		}
		$model 	= $this->getModel();
		$items 	= $model->getItems($setId);
		$set	= $model->getSet($setId);
		if (count($model->getErrors()) > 0) {
			echo "<error>Error reading model</error>";
			exit;
		}
		//Prepare data global
		$visible = $set->navi_dots_conf->get('dotvisible');
		$panelvisible = $set->navi_dots_conf->get('panelvisible');
		$switchmethod = $set->navi_dots_conf->get('switchmethod');
		$showOnMouseOver = $set->navi_dots_conf->get('showOnMouseOver');
		$align = $set->navi_dots_conf->get('dotalign');
		$urlNormalState = $set->navi_dots_conf->get('doturlnormalstate');
		$urlOverState = $set->navi_dots_conf->get('doturloverstate');
		$preloaderShowBar = $set->preloader_conf->get('preloadershow');
		$preloaderShowPercent = $set->preloader_conf->get('preloadershowpercent');
		$preloaderAlign = $set->preloader_conf->get('preloaderalign');
		//TODO: htmlspecialchars($value);
		echo '<config>';
		echo '	<global>';
		echo '		<navi_dots visible="'.$visible.'" showOnMouseOver="'.$showOnMouseOver.'" align="'.$align.'" urlOverState="'.$urlOverState.'" urlNormalState="'.$urlNormalState.'"/>';
		echo '		<navi_panel visible="'.$panelvisible.'" align="'.$align.'" valign="bottom" layoutPath="/modules/mod_skel/assets/gfx/panel"/>';
		echo '		<bitmapVisibleTime time = "'.$set->visible_time.'"/>';
		echo '		<switchEffect method="fade" /> <!-- fade / none -->';
		echo '		<preloader showBar = "'.$preloaderShowBar.'" showPercent = "'.$preloaderShowPercent.'" align="'.$preloaderAlign.'"/>';
		//print_r($session->get('skel.stamp.elid'));
		echo '		<sliderInfo id="slider1" elementIdStart="'.$session->get('skel.stamp.elid').'" timeStart="'.$session->get('skel.stamp.time').'"/>';
		echo '		<notify url="index.php?option=com_skel&amp;task=data.store" wait="10" /> ';
		echo '	</global>';
		echo '	<pictures>';
		foreach ($items as $item) {
			// prepare item data
			$url = $item->attributes->get('url');
			$picturetype = $item->attributes->get('picturetype');
			$clickurl = $item->attributes->get('clickurl');
			$visible_time = $item->attributes->get('visible_time');
			$text = $item->attributes->get('text');
			$clickurl = $item->attributes->get('clickurl');
			$target = $item->attributes->get('target');
			$scale = $item->attributes->get('scale');
			echo '	<item id="'.$item->id.'" url="'.$url.'" time="'.$visible_time.'" picture_type="'.$picturetype.'" clickURL="'.$clickurl.'" target="'.$target.'" scale="'.$scale.'">';
			echo '		<text>'.$text.'</text>';
			echo '	</item>';
		}
		echo '	</pictures>';
		echo '</config>';
		exit;
	}
	public function getModel($name = 'Data', $prefix = 'SkelModel', $config = array()){
		return parent::getModel($name, $prefix, $config);
	}

}

