<?php
function pagination_list_footer(&$list)
{
	$app = JFactory::getApplication();
	
	// Initialise variables.
	$limits = array();
	
	// Make the option list.
	$limits[] = JHtml::_('select.option', "10");
	$limits[] = JHtml::_('select.option', "20");
	$limits[] = JHtml::_('select.option', '50', JText::_('J50'));
	$limits[] = JHtml::_('select.option', '0', JText::_('JALL'));
	if (preg_match('/value="0" selected="selected"/', $list['limitfield'])) {
		$selected = 0;	
	} else {
		$selected = $list['limit'];
	}
	
	// Build the select list.
	if ($app->isAdmin())
	{
		$html = JHtml::_(
				'select.genericlist',
				$limits,
				$list['prefix'] . 'limit',
				'class="inputbox" size="1" onchange="Joomla.submitform();"',
				'value',
				'text',
				$selected
		);
	}
	else
	{
		$html = JHtml::_(
				'select.genericlist',
				$limits,
				$list['prefix'] . 'limit',
				'class="inputbox" size="1" onchange="this.form.submit()"',
				'value',
				'text',
				$selected
		);
	}
	$list['limitfield'] = $html;
	
	
	$rethtml = "<div class=\"list-footer\">\n";
	
	$rethtml .= "\n<div class=\"limit\"><span>" . JText::_('JGLOBAL_DISPLAY_NUM') .'</span>'. $list['limitfield'] . "</div>";
	$rethtml .= $list['pageslinks'];
	$rethtml .= "\n<div class=\"counter\">" . $list['pagescounter'] . "</div>";
	
	$rethtml .= "\n<input type=\"hidden\" name=\"" . $list['prefix'] . "limitstart\" value=\"" . $list['limitstart'] . "\" />";
	$rethtml .= "\n</div>";
	
	return $rethtml;
}