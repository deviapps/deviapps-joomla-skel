<?php
defined('_JEXEC') or die;
JHTML::_('behavior.mootools');
$templateUrl = $this->baseurl."/templates/".$this->template."/";
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl-pl" lang="pl-pl" dir="ltr">
<head>
<jdoc:include type="head" />
  <meta http-equiv="Expires" CONTENT="0" />
  <meta http-equiv="Cache-Control" CONTENT="no-cache" />
  <meta http-equiv="Pragma" CONTENT="no-cache" />
<meta name="author" content="" />
<meta name="robots" content="index,follow,all" />
<meta name="revisit-after" content="10 days" />
<meta http-equiv="content-language" content="pl" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl."/templates/system/";?>css/system.css" />
<link rel="Stylesheet" type="text/css" href="<?php echo $templateUrl;?>css/reset.css" />
<link rel="Stylesheet" type="text/css" href="<?php echo $templateUrl;?>css/layout.css" />
	
<script type="text/javascript" src="<?php echo $templateUrl;?>javascript/jquery-1.6.4.js"></script>
<script type="text/javascript" src="<?php echo $templateUrl;?>javascript/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript">
  $.noConflict();
  jQuery(document).ready(function($) {
    // Code that uses jQuery's $ can follow here.
	if (! navigator.cookieEnabled) {
		$('#cookieError').css('display','block');
	} 
  });
</script>
</head>

<body>
	<div class="container">
		<jdoc:include type="message" />
		<noscript>
			<div class="errorrow" style="padding: 10px 0 10px 100px;">Do poprawnego działania aplikacji potrzebujesz włączonej obsługi JavaScript</div>
		</noscript>
		<div class="errorrow" style="padding: 10px 0 10px 100px;display:none;" id="cookieError">Do poprawnego działania aplikacji potrzebujesz włączonej obsługi Cookies</div>
		<div class="userinfo">
			<jdoc:include type="modules" name="userinfo" style="none" />
		</div>
		<div class="middle fixedempty">
			<jdoc:include type="component" />
		</div>	
		<div class="bottom"><img src="<?php echo $templateUrl;?>images/box-shadow.png" alt="" width="980" height="18px" /></div>
	</div>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>