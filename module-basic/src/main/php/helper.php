<?php
/**
 * @author		Cyprian Sniegota
 * @package		skel.module
 * @copyright	Copyright (C) 2012- deviapps.com Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');

abstract class modSkelHelper
{
	

	public static function getCrumbs(&$params)
	{
		$app = JFactory::getApplication();
		$uri = JFactory::getUri();
		$path = $uri->getPath();
		if (strpos($path,'/') === 0) {
			$path = substr($path,1);
		}
		$view =  $app->input->get('view');
		$layout =  $app->input->get('layout');
		$data = array("here","and here");
		return join(" &raquo; ",$data);
	}

}