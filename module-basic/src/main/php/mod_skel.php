<?php
// no direct access
defined('_JEXEC') or die;
require_once dirname(__FILE__).'/helper.php';
$user = JFactory::getUser();

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
if ($user->id > 0) {
	$breadcrumbs = modSkelHelper::getCrumbs($params);
	require JModuleHelper::getLayoutPath('mod_skel',$params->get('layout', 'default'));
}
